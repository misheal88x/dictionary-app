import 'dart:convert';

class TermObject {
  String arabicTerm;
  String englishTerm;
  String trasliteration;
  bool isGreen;
  bool isYellow;

  TermObject(this.arabicTerm, this.englishTerm, this.trasliteration,
      this.isGreen, this.isYellow);

  factory TermObject.fromJson(Map<String, dynamic> jsonData) {
    return TermObject(
      jsonData["arabicTerm"],
      jsonData["englishTerm"],
      jsonData["trasliteration"],
      jsonData["isGreen"],
      jsonData["isYellow"],
    );
  }

  static Map<String, dynamic> toMap(TermObject term) {
    return {
      'arabicTerm': term.arabicTerm,
      'englishTerm': term.englishTerm,
      'trasliteration': term.trasliteration,
      'isGreen': term.isGreen,
      'isYellow': term.isYellow,
    };
  }

  static String encode(List<TermObject> terms) => jsonEncode(
        terms
            .map<Map<String, dynamic>>((term) => TermObject.toMap(term))
            .toList(),
      );

  static List<TermObject> decode(String terms) =>
      (json.decode(terms) as List<dynamic>)
          .map<TermObject>((item) => TermObject.fromJson(item))
          .toList();
}
