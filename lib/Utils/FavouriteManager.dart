import 'package:dictionaryapp/Utils/BaseFunctions.dart';
import 'package:dictionaryapp/Utils/TermObject.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

Future<List<TermObject>> getFavourites() async {
  var sharedPreferences = await SharedPreferences.getInstance();
  String json = sharedPreferences.getString("fav") ?? "[]";
  List<TermObject> l = TermObject.decode(json);
  return l;
}

Future setFavourite(List<TermObject> list) async {
  var sharedPreferences = await SharedPreferences.getInstance();
  String json = TermObject.encode(list);
  sharedPreferences.setString("fav", json);
  return list;
}

void addToFavourite(TermObject t) async {
  getFavourites().then((value) {
    value.add(t);
    setFavourite(value);
  });
}

Future removeFromFavourite(TermObject t) async {
  getFavourites().then((value) async {
    List<TermObject> list = value;
    list.removeWhere((element) => element.arabicTerm == t.arabicTerm);
    await setFavourite(list);
    return t;
  });
}

Future<bool> isFavourite(TermObject t) async {
  bool found = false;
  await getFavourites().then((value) {
    if (value.length > 0) {
      for (TermObject to in value) {
        if (to.arabicTerm == t.arabicTerm) {
          found = true;
          break;
        }
      }
    }
  });
  return found;
}
