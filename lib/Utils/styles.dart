import 'package:flutter/material.dart';

const mediumBlackBold =
    TextStyle(color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.bold);
const smallBlack = TextStyle(color: Colors.black, fontSize: 14.0);
const smallGrey = TextStyle(color: Colors.grey, fontSize: 14.0);
const smallBlue = TextStyle(color: Colors.blue, fontSize: 14.0);
