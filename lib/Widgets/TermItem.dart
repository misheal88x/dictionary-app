import 'package:dictionaryapp/Utils/BaseFunctions.dart';
import 'package:dictionaryapp/Utils/FavouriteManager.dart';
import 'package:dictionaryapp/Utils/TermObject.dart';
import 'package:dictionaryapp/Utils/styles.dart';
import 'package:dictionaryapp/Widgets/CustomCircle.dart';
import 'package:dictionaryapp/Widgets/OutlineCircle.dart';
import 'package:flutter/material.dart';

class TermItem extends StatelessWidget {
  TermObject term;
  TermItem(this.term);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10.0, left: 15.0, right: 15.0),
      child: Material(
        shadowColor: Colors.grey,
        elevation: 2.0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            side: BorderSide.none),
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      term.isGreen
                          ? CustomCircle(
                              color: Colors.green,
                            )
                          : term.isYellow
                              ? CustomCircle(color: Colors.yellow)
                              : OutlineCircle(),
                      Text(
                        term.arabicTerm,
                        style: mediumBlackBold,
                        textAlign: TextAlign.left,
                      )
                    ],
                  ),
                  Text(
                    term.trasliteration,
                    style: smallBlack,
                  )
                ],
              ),
              SizedBox(
                height: 5.0,
              ),
              Text(
                "Translation",
                textAlign: TextAlign.left,
                style: smallBlue,
              ),
              Text(
                term.englishTerm,
                style: smallGrey,
                textAlign: TextAlign.left,
              ),
              SizedBox(
                height: 2.0,
              ),
              Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                  onTap: () {
                    isFavourite(term).then((value) {
                      if (value) {
                        showToast(context, "Removed from favourite");
                        removeFromFavourite(term);
                      } else {
                        showToast(context, "Added to favourite");
                        addToFavourite(term);
                      }
                    });
                  },
                  child: Icon(
                    Icons.star,
                    color: Colors.yellow,
                    size: 20.0,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
