import 'package:flutter/material.dart';

class OutlineCircle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      shape: CircleBorder(side: BorderSide(color: Colors.black, width: 2.0)),
      fillColor: Colors.white,
      constraints: BoxConstraints.tightFor(width: 35.0, height: 35.0),
      onPressed: () {},
    );
  }
}
