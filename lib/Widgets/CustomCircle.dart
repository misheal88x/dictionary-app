import 'package:flutter/material.dart';

class CustomCircle extends StatelessWidget {
  final color;
  CustomCircle({this.color});
  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      shape: CircleBorder(),
      fillColor: color,
      constraints: BoxConstraints.tightFor(width: 35.0, height: 35.0),
      onPressed: () {},
    );
  }
}
