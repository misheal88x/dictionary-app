import 'package:dictionaryapp/Utils/styles.dart';
import 'package:flutter/material.dart';

class CustomSearchField extends StatelessWidget {
  final Function onSubmitted;
  final Function onChanged;
  final String hint;

  CustomSearchField({this.onChanged, this.onSubmitted, this.hint});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: TextField(
        style: smallBlack,
        textInputAction: TextInputAction.search,
        onSubmitted: onSubmitted,
        onChanged: onChanged,
        decoration: InputDecoration(
            filled: true,
            fillColor: Colors.grey[200],
            prefixIcon: Icon(
              Icons.search,
              color: Colors.grey,
            ),
            contentPadding: EdgeInsets.all(5.0),
            hintText: hint,
            border: OutlineInputBorder(
                borderSide: BorderSide.none,
                borderRadius: BorderRadius.all(Radius.circular(25.0)))),
      ),
    );
  }
}
