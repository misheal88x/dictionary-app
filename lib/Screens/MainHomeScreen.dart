import 'package:dictionaryapp/Utils/Database.dart';
import 'package:dictionaryapp/Utils/TermObject.dart';
import 'package:dictionaryapp/Widgets/CustomSearchField.dart';
import 'package:dictionaryapp/Widgets/TermItem.dart';
import 'package:flutter/material.dart';

class MainHomeScreen extends StatefulWidget {
  @override
  _MainHomeScreenState createState() => _MainHomeScreenState();
}

class _MainHomeScreenState extends State<MainHomeScreen> {
  List<TermObject> list = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.only(top: 15.0),
        child: ListView.builder(
          itemBuilder: (context, position) {
            TermObject o = list[position];
            return TermItem(o);
          },
          itemCount: list.length,
          shrinkWrap: true,
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      list = datasource;
    });
  }
}
