import 'package:dictionaryapp/Utils/FavouriteManager.dart';
import 'package:dictionaryapp/Utils/TermObject.dart';
import 'package:dictionaryapp/Widgets/TermItem.dart';
import 'package:flutter/material.dart';

class MainFavouriteScreen extends StatefulWidget {
  @override
  _MainFavouriteScreenState createState() => _MainFavouriteScreenState();
}

class _MainFavouriteScreenState extends State<MainFavouriteScreen> {
  List<TermObject> list = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.only(top: 15.0),
        child: ListView.builder(
          itemBuilder: (context, position) {
            TermObject o = list[position];
            return TermItem(o);
          },
          itemCount: list.length,
          shrinkWrap: true,
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    getFavourites().then((value) {
      setState(() {
        list = value;
      });
    });
  }
}
