import 'package:dictionaryapp/Screens/MainDailyCardScreen.dart';
import 'package:dictionaryapp/Screens/MainFavouriteScreen.dart';
import 'package:dictionaryapp/Screens/SearchScreen.dart';
import 'package:flutter/material.dart';

import 'MainHomeScreen.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int index = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        centerTitle: true,
        title: Text('Dictionary'),
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return SearchScreen();
              }));
            },
            child: Icon(
              Icons.search,
              color: Colors.white,
            ),
          )
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: Container(
              width: double.infinity,
              height: double.infinity,
              color: Colors.white,
              child: selectedScreen(index),
            ),
          ),
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(5.0),
            decoration: BoxDecoration(
                color: Colors.blue[900],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10.0),
                    topRight: Radius.circular(10.0))),
            child: Row(
              children: [
                Expanded(
                  child: FlatButton(
                    child: Icon(
                      Icons.home,
                      color: index == 1 ? Colors.white : Colors.grey,
                      size: index == 1 ? 30.0 : 25.0,
                    ),
                    onPressed: () {
                      if (index != 1) {
                        setState(() {
                          index = 1;
                        });
                      }
                    },
                  ),
                ),
                Expanded(
                  child: FlatButton(
                    child: Icon(
                      Icons.date_range,
                      color: index == 2 ? Colors.white : Colors.grey,
                      size: index == 2 ? 30.0 : 25.0,
                    ),
                    onPressed: () {
                      if (index != 2) {
                        setState(() {
                          index = 2;
                        });
                      }
                    },
                  ),
                ),
                Expanded(
                  child: FlatButton(
                    child: Icon(
                      Icons.stars,
                      color: index == 3 ? Colors.white : Colors.grey,
                      size: index == 3 ? 30.0 : 25.0,
                    ),
                    onPressed: () {
                      if (index != 3) {
                        setState(() {
                          index = 3;
                        });
                      }
                    },
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget selectedScreen(int i) {
    if (i == 1) {
      return MainHomeScreen();
    } else if (i == 2) {
      return MainDailyCardScreen();
    } else if (i == 3) {
      return MainFavouriteScreen();
    } else {
      return Container();
    }
  }
}
