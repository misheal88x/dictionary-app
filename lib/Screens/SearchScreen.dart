import 'package:dictionaryapp/Utils/Database.dart';
import 'package:dictionaryapp/Utils/TermObject.dart';
import 'package:dictionaryapp/Widgets/CustomSearchField.dart';
import 'package:dictionaryapp/Widgets/TermItem.dart';
import 'package:flutter/material.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  String searchText = "";
  List<TermObject> list = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        centerTitle: true,
        title: Text("Search"),
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 15.0),
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.0),
              child: CustomSearchField(
                hint: 'Search..',
                onChanged: (v) {
                  if (v.toString().length == 0) {
                    setState(() {
                      list = [];
                    });
                  }
                  searchText = v;
                },
                onSubmitted: (v) {
                  if (searchText != "") {
                    setState(() {
                      list = search(searchText);
                    });
                  }
                },
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            ListView.builder(
              itemBuilder: (context, position) {
                TermObject o = list[position];
                return TermItem(o);
              },
              itemCount: list.length,
              shrinkWrap: true,
            )
          ],
        ),
      ),
    );
  }
}
